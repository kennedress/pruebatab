@file:Suppress("DEPRECATION")

package com.example.tablayoutexample

import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.android.material.tabs.TabLayout

import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(),
    BlankFragment1.OnFragmentInteractionListener,
    BlankFragment2.OnFragmentInteractionListener,
    BlankFragment3.OnFragmentInteractionListener{

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        tbHome.addTab(tbHome.newTab().setText("oli").setIcon(R.drawable.ic_brightness_2_red_24dp))
        tbHome.addTab(tbHome.newTab().setText("oli x2").setIcon(R.drawable.ic_brightness_2_black_24dp))
        tbHome.addTab(tbHome.newTab().setText("oli x3").setIcon(R.drawable.ic_brightness_2_black_24dp))


        val adapter = VPAdapter(this, supportFragmentManager, tbHome.tabCount)
        vpHome.adapter = adapter

        vpHome.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tbHome))

        tbHome!!.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                vpHome!!.currentItem = tab.position
                tab.setIcon(R.drawable.ic_brightness_2_red_24dp)
            }
            override fun onTabUnselected(tab: TabLayout.Tab) {
                tab.setIcon(R.drawable.ic_brightness_2_black_24dp)
            }
            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })
    }

    override fun onFragmentInteraction(uri: Uri) { }
}

